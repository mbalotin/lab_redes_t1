/*-------------------------------------------------------------*/
/* Exemplo Socket Raw - Captura pacotes recebidos na interface */
/*-------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <string.h>
#include <unistd.h>

/* Diretorios: net, netinet, linux contem os includes que descrevem */
/* as estruturas de dados do header dos protocolos   	  	        */

#include <net/if.h>  //estrutura ifr
#include <netinet/ether.h> //header ethernet
#include <netinet/in.h> //definicao de protocolos
#include <arpa/inet.h> //funcoes para manipulacao de enderecos IP
#include <netinet/ip.h>    //Provides declarations for ip header

#include <netinet/in_systm.h> //tipos de dados

#define BUFFSIZE 1518

// Atencao!! Confira no /usr/include do seu sisop o nome correto
// das estruturas de dados dos protocolos.

typedef struct
{
    int qtd;
    char *data;
} node_t;

typedef struct
{
    node_t *nodes;
    int len;
    int size;
} heap_t;

int cmpfunc (const void * a, const void * b)
{
    node_t e1 = *(node_t *)a;
    node_t e2 = *(node_t *)b;

    return e2.qtd - e1.qtd;
}

void add(heap_t *h, char *data)
{
    int i = 0;

    if (h->len == 0)
    {
        h->nodes[i].qtd = 1;
        h->nodes[i].data = data;
        h->len++;
        return ;
    }

    for (i = 0; i < h->len; i++)
    {
        if (strcmp(h->nodes[i].data, data) == 0)
        {
            h->nodes[i].qtd += 1;
            qsort(h->nodes, h->len, sizeof(node_t), cmpfunc);
            return ;
        }
    }

    h->nodes = (node_t *)realloc(h->nodes, (h->len +1) * sizeof (node_t));

    i = h->len;
    h->nodes[i].qtd = 1;
    h->nodes[i].data = data;
    h->len++;
    qsort(h->nodes, h->len, sizeof(node_t), cmpfunc);

}

unsigned char buff1[BUFFSIZE];
heap_t *top5ip;
heap_t *top5portTCP;
heap_t *top5portUDP;
heap_t *top5site;

int sockd;
int on;
struct ifreq ifr;

long min_package_size = 999999;
long max_package_size = 0;

long total_package_size = 0 ;
int package_count = 0;

long total_ARP_requests = 0;
long total_ARP_replies = 0;

long total_ICMP_requests = 0;
long total_ICMP_replies = 0;

long total_TCP = 0;
long total_UDP = 0;
long started_TCP_connections = 0;

long total_HTTP = 0;
long total_DNS = 0;
long total_QUIC = 0;
long total_SSH = 0;

void print_separator()
{
    printf("-------------------------------------------------\n");
}

void print_results(long package_size, char interface[])
{
    int j;
    system("clear");
    print_separator();
    printf("--------------------%9s--------------------\n", interface);
    print_separator();
    // Package sizes
    printf(" MIN package size %6ld bytes\n", min_package_size);
    printf(" MAX package size %6ld bytes\n", max_package_size);
    printf(" AVG package size %6ld bytes\n", total_package_size / package_count);
    // percentage of ARP requests and replyes
    print_separator();
    printf("%6ld ARP requests, %5.2f%% of total.\n", total_ARP_requests, (double)total_ARP_requests/package_count * 100);
    printf("%6ld ARP replies,  %5.2f%% of total.\n", total_ARP_replies, (double)total_ARP_replies/package_count * 100);
    // percentage and qt of ICMP
    print_separator();
    printf("%6ld ICMP packages,      %5.2f%% of total.\n", total_ICMP_requests + total_ICMP_replies, (double)(total_ICMP_requests+total_ICMP_replies)/package_count * 100);
    printf("%6ld ICMP Echo Requests, %5.2f%% of total.\n", total_ICMP_requests, (double)total_ICMP_requests/package_count * 100);
    printf("%6ld ICMP Echo Replies,  %5.2f%% of total.\n", total_ICMP_replies, (double)total_ICMP_replies/package_count * 100);
    // TOP 5 IPs
    print_separator();
    printf(" TOP 5 Ips\n");
    for (j = 0; j < top5ip->len; j++)
    {
        if (j < 5)
        {
            printf(" IP #%d: %s\n", j+1, top5ip->nodes[j].data);
        }
        else break;
    }
    // TCP and UDP count
    print_separator();
    printf("%6ld UDP packages, %5.2f%% of total.\n", total_UDP, (double)total_UDP / package_count * 100);
    printf("%6ld TCP packages, %5.2f%% of total.\n", total_TCP, (double)total_TCP / package_count * 100);
    printf("%6ld TCP connections started.\n", started_TCP_connections);
    // TOP TCP ports
    print_separator();
    printf(" TOP 5 TCP ports: \n \n");
    for (j = 0; j < top5portTCP->len; j++)
    {
        if (j < 5)
        {
            printf(" Port #%d: %s\n", j+1, top5portTCP->nodes[j].data);
        }
        else break;
    }

    // TOP UDP ports
    print_separator();
    printf(" TOP 5 UDP ports: \n \n");
    for (j = 0; j < top5portUDP->len; j++)
    {
        if (j < 5)
        {
            printf(" Port #%d: %s\n", j+1, top5portUDP->nodes[j].data);
        }
        else break;
    }

    // HTTP, DNS count
    print_separator();
    printf("%6ld HTTP packages,   %5.2f%% of total.\n", total_HTTP, (double)total_HTTP / package_count * 100);
    printf("%6ld DNS packages,    %5.2f%% of total.\n", total_DNS, (double)total_DNS / package_count * 100);
    printf("%6ld QUIC packages,   %5.2f%% of total.\n", total_QUIC, (double)total_QUIC / package_count * 100);
    printf("%6ld SSH packages,    %5.2f%% of total.\n", total_SSH, (double)total_SSH / package_count * 100);
    // TOP 5 accesed sites
    print_separator();
    printf(" TOP 5 sites\n");
    for (j = 0; j < top5site->len; j++)
    {
        if (j < 5)
        {
            printf(" Site #%d: %s\n", j+1, top5site->nodes[j].data);
        }
        else break;
    }

    print_separator();
    // remove
    printf("Additional info:\n");
    printf("    Last package size %6ld bytes\n", package_size);
    printf("    MAC Destino: %x:%x:%x:%x:%x:%x \n", buff1[0],buff1[1],buff1[2],buff1[3],buff1[4],buff1[5]);
    printf("    MAC Origem:  %x:%x:%x:%x:%x:%x \n", buff1[6],buff1[7],buff1[8],buff1[9],buff1[10],buff1[11]);
}

int main(int argc,char *argv[])
{
    if (argc == 1)
    {
        printf("Informe o nome da interface que deseja monitorar por parâmetro.\n");
        printf("  sudo ./monitor <interface>\n");
        exit(1);
    }
    else if (argc > 2)
    {
        printf("Apenas uma interface aceita para monitoramento.\n");
        printf("  sudo ./monitor <interface>\n");
        exit(1);
    }

    /* Criacao do socket. Todos os pacotes devem ser construidos a partir do protocolo Ethernet. */
    /* De um "man" para ver os parametros.*/
    /* htons: converte um short (2-byte) integer para standard network byte order. */
    if((sockd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0)
    {
        printf("Erro na criacao do socket, tente executar como root.\n");
        exit(1);
    }

    // O procedimento abaixo eh utilizado para "setar" a interface em modo promiscuo
    strcpy(ifr.ifr_name, argv[1]);
    if(ioctl(sockd, SIOCGIFINDEX, &ifr) < 0)
        printf("erro no ioctl!");
    ioctl(sockd, SIOCGIFFLAGS, &ifr);
    ifr.ifr_flags |= IFF_PROMISC;
    ioctl(sockd, SIOCSIFFLAGS, &ifr);

    //arrays que armazenam os top 5
    char * value;
    int porta;

    top5ip = (heap_t *)calloc(1, sizeof(heap_t));
    top5portTCP = (heap_t *)calloc(1, sizeof(heap_t));
    top5portUDP = (heap_t *)calloc(1, sizeof(heap_t));
    top5site = (heap_t *)calloc(1, sizeof(heap_t));

    top5ip->nodes = (node_t *)calloc(1, sizeof(node_t));
    top5portTCP->nodes = (node_t *)calloc(1, sizeof(node_t));
    top5portUDP->nodes = (node_t *)calloc(1, sizeof(node_t));
    top5site->nodes = (node_t *)calloc(1, sizeof(node_t));

    // recepcao de pacotes
    system("clear");
    while (1)
    {
        //memset(value, 0, sizeof(char *));
        long package_size = recv(sockd,(char *) &buff1, sizeof(buff1), 0x0);
        package_count++;
        total_package_size += package_size;
        if (min_package_size > package_size)
            min_package_size = package_size;
        if (max_package_size < package_size)
            max_package_size = package_size;
        if (buff1[12] == 0x08 && buff1[13] == 0x06)  // Test ARP protocol
        {
            // Reply or Request
            if (buff1[20] == 0x00)
                if (buff1[21] == 0x01)// ARP request
                    total_ARP_requests++;
            if (buff1[21] == 0x02)// ARP Reply
                total_ARP_replies++;
        }
        else if (buff1[12] == 0x08 && buff1[13] == 0x00)    // IP Protocol
        {
            asprintf(&value, "%d.%d.%d.%d", buff1[30],buff1[31],buff1[32],buff1[33]);
            add(top5ip, value);

            if (buff1[23] == 0x01)  // Test ICMP
            {
                if (buff1[34] == 0x08) // ICMP Echo request
                    total_ICMP_requests++;
                else if (buff1[34] == 0x00)// ICMP Echo reply
                    total_ICMP_replies++;
            }
            else if (buff1[23] == 0x06)    // Test TCP
            {
                total_TCP++;
                porta = buff1[36]<<8|buff1[37];
                asprintf(&value, "%d", porta);
                add(top5portTCP, value);

                //Check started TCP connection by lokking at the SYN(2nd) flag on the TCP flags byte
                // 0x02 = 0000 0010
                if ((buff1[47] & 0x02) != 0)
                    started_TCP_connections++;

                // Checks if HTTP - Origin or Destination Port is 80 (0x0050)
                if ((buff1[34] == 0x00 && buff1[35] == 0x50) || (buff1[36] == 0x00 && buff1[37] == 0x50))
                {
                    total_HTTP++;
                    int a;
                    for (a = 83; a < 108; a++)
                    {
                        asprintf(&value, "%c", buff1[a]);
                    }

                    add(top5site, value);
                }
                // Checks if SSH - Origin or Destination Port is 22 (0x0016)
                else if ((buff1[34] == 0x00 && buff1[35] == 0x16) || (buff1[36] == 0x00 && buff1[37] == 0x16))
                    total_SSH++;
            }
            else if (buff1[23] == 0x11)    // Test UDP
            {
                total_UDP++;
                porta = buff1[36]<<8|buff1[37];
                asprintf(&value, "%d", porta);
                add(top5portUDP, value);

                // Checks if DNS - Origin or Destination Port is 53 (0x0035)
                if ((buff1[34] == 0x00 && buff1[35] == 0x35) || (buff1[36] == 0x00 && buff1[37] == 0x35))
                    total_DNS++;
                // Checks if QUIC - Origin or Destination Port is 443 (0x01bb)
                else if ((buff1[34] == 0x01 && buff1[35] == 0xbb) || (buff1[36] == 0x01 && buff1[37] == 0xbb))
                    total_QUIC++;
            }
        }

        print_results(package_size, argv[1]);
    }
}


